package com.michael.websocket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Server {

	public static int numberOfConnections = 1;
	public static Logger logger = Logger.getLogger("websocket-server");
	public static FileHandler fh;
	public static SimpleFormatter formatter = new SimpleFormatter();  

	public static void main(String[] args) throws Exception {

        // This block configure the logger with handler and formatter  
        fh = new FileHandler("/Users/michael/Development/personal/learnJavaSocket/server.log");  
        logger.addHandler(fh);
        fh.setFormatter(formatter);  

		ServerSocket serSocket = new ServerSocket(9999);

		while(true) {
			new ServerThread(serSocket.accept()).start();
		}
	}
}

class ServerThread extends Thread {

	Socket sock;

	InputStream in = null;
	OutputStream out = null;
	private int connectionNumber = 0;

	public ServerThread(Socket sock) throws IOException {
		this.sock = sock;
		in = sock.getInputStream();
		out = sock.getOutputStream();

		connectionNumber = Server.numberOfConnections++;
		System.out.println("Server thread initialized - " + connectionNumber);

		Server.logger.info("Server thread initialized - " + connectionNumber); 
	}

	public void run() {

		try {
			while(true) {

				byte request[] = new byte[1024];
				in.read(request);

				String logMessage = "Message received for Connection [" + connectionNumber + "] is {" + new String(request).trim() + "}";
				Server.logger.info(logMessage); 
				System.out.println(logMessage);
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}